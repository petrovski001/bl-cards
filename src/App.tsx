import { useEffect, useState } from "react";
import Card from "./components/Card";
import "./App.css";

function App() {
  const [cards, setCards] = useState<Array<string>>([]);
  const [colors, setColors] = useState<Array<string>>([
    "#91A6FF",
    "#ff88dc",
    "#FAFF7F",
    "#FF5154",
    "#57A773",
  ]);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const shuffleCards = (): any => {
    const cards = ["🪞✨", "🚽🪠", "🧺🧼", "🗑♻️", "🍆💦"];
    const colours = ["#91A6FF", "#ff88dc", "#FAFF7F", "#FF5154", "#57A773"];
    for (let i = cards.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [cards[i], cards[j]] = [cards[j], cards[i]];
      [colors[i], colors[j]] = [colors[j], colors[i]];
    }

    return { cards, colours };
  };

  useEffect(() => {
    if (cards.length === 0) {
      const shuffledCards = shuffleCards();
      setCards(shuffledCards.cards);
      setColors(shuffledCards.colours);
    }
  }, [cards, colors, shuffleCards]);

  const renderCards = () => {
    return cards.map((card, i) => {
      return <Card value={card} color={colors[i]} key={card} />;
    });
  };

  return <div className="grid">{renderCards()}</div>;
}

export default App;
