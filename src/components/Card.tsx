import { useState } from "react";
import "./card.css";

type ICardProps = {
  value: string;
  color: string;
};

export default function Card({ value, color }: ICardProps) {
  const [isFlipped, setFlip] = useState(false);

  const flipCard = () => {
    !isFlipped ? setFlip(true) : setFlip(false);
  };

  return (
    <div>
      <div className={`card`} onClick={flipCard} >
        <div className={`content ${isFlipped ? "rotate" : ""}`}>
          <div className="front" style={{ backgroundColor: color }} />
          <div className="back">{value}</div>
        </div>
      </div>
    </div>
  );
}
